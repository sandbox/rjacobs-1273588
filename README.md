Salesforce Error Handling
=========================

### About

This module provides enhanced error handling/logging for failed Salesforce exports.
It monitors salesforce exports for errors and logs/queues any errors
it detects for later processing. It also implements cron processing of errors.

### Installation

Navigate to admin/build/modules and click "Enable".

** Note: This module is not compatible with `sf_queue`. **

### Configuration

Configure the settings for this module on admin/settings/salesforce.

The error log is viewable at `admin/settings/sf_errors`.

### Credits

This module was originally concieved and authored by Ryan Jacobs (rjacobs).

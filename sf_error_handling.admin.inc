<?php

/**
 * Callback to build the form that will display the queue and allow admins to
 * trigger a processing of the queue or clear the queue.
 */
function sf_error_handling_show_queue() {
  $sf_errors_count = db_result(db_query("SELECT count(created) FROM {salesforce_errors}"));
  $header          = array(t('Queue ID'), t('Operation'), t('Created'), t('Updated'), t('Error Count'), t('Manual Attempts'), t('Cron Attempts'), t('Drupal Type'), t('Drupal ID'), t('SF Type'), t('SF ID'), t('Error Message/Details'));
  $rows            = array();
  $sf_error_items  = db_query("SELECT * FROM {salesforce_errors} ORDER BY id");
  // Structure a themed table with the values from the database.
  while ($sf_error_item = db_fetch_object($sf_error_items)) {
    $rows[] = array(
      'data' => array(
        $sf_error_item->id,
        $sf_error_item->sf_op,
        date("r", $sf_error_item->created),
        date("r", $sf_error_item->updated),
        $sf_error_item->error_count,
        $sf_error_item->manual_attempts,
        $sf_error_item->cron_attempts,
        $sf_error_item->drupal_type,
        $sf_error_item->oid,
        $sf_error_item->sf_type,
        $sf_error_item->sfid,
        $sf_error_item->error_msg,
      ),
    );
  }
  $output = theme('table', $header, $rows);
  if ($sf_errors_count > 0) {
    if ($sf_errors_count == 1) {
      $output .= "There is 1 item in the error queue.";
    }
    else {
      $output .= "There are " . count($rows) . " items in the error queue.";
    }
  }
  else {
    $output .= "There are no items in the error queue.";
  }
  $output .= "<br/><br/>";
  $form = array();
  $form['sf_errors'] = array(
    '#type' => 'markup',
    '#value' => t('The followng exports have failed one-or-more times and are logged below. You may attempt to manually export these items by choosing "Process Queue" below.') . "<br/><br/>" . $output,
  );
  $form['buttons']['reset'] = array('#type' => 'submit', '#value' => t('Process queue'));
  if (!empty($_POST) && form_get_errors()) {
    drupal_set_message(t('The settings have not been saved because of the errors.'), 'error');
  }
  $form['sf_error_handling_clear'] = array(
    '#type' => 'markup',
    '#value' => l(t('Clear the queue'), 'admin/settings/sf_errors/clear'),
  );
  $form['#submit'][] = 'sf_error_handling_run_queue';
  $form['#theme'] = 'system_settings_form';
  return $form;
}

/**
 * Callback to process the error queue via form.
 */
function sf_error_handling_run_queue($form, &$form_state) {
  $count = sf_error_handling_process_queue("manual");
  if ($count === FALSE) {
    drupal_set_message(t("The error queue is empty"), 'status');
    return;
  }
  if ($count['success'] >= 1) {
    drupal_set_message(t("@success exports were successfully processed and @fail failed", array('@success' => $count['success'], '@fail' => $count['fail'])), 'status');
  }
  else {
    drupal_set_message(t("No exports could be processed"), 'error');
  }
}

/**
 * Callbacks to clear the queue.
 */
function sf_error_handling_clear_queue() {
  $question = "Are you sure you want to clear the Salesforce export error queue?";
  return confirm_form(array(), check_plain($question), 'admin/settings/sf_errors');
}

/**
 * Submit handler for clearing the SF Error queue.
 */
function sf_error_handling_clear_queue_submit() {
  $count = 0;
  $sf_error_items = db_query("SELECT * FROM {salesforce_errors} ORDER BY id");
  while ($sf_error_item = db_fetch_object($sf_error_items)) {
    db_query('DELETE FROM {salesforce_errors} WHERE id = %d', $sf_error_item->id);
    $count++;
  }
  drupal_set_message(t("@count items were removed from the queue.", array('@count' => $count)));
  drupal_goto('admin/settings/sf_errors');
}

/**
 * Implementation of hook_form_FORM_ID_alter().
 *
 * Adds error handling settings to the Salesforce API settings form.
 */
function sf_error_handling_form_salesforce_api_settings_form_alter(&$form, $form_state) {
  $form['sf_error_handling'] = array(
    '#type' => 'fieldset',
    '#title' => t('Error handling settings'),
    '#description' => t('Configure the handling of errors and the ') . '<strong>' . l(t('export error queue'), 'admin/settings/sf_errors') . '</strong>',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => -1,
  );
  $form['sf_error_handling']['sf_error_handling_per_process_number'] = array(
    '#type' => 'select',
    '#title' => t('Maximum number of items to handle each time the error queue is processed'),
    '#description' => t('How many items should be handled each time the error queue is processed (either manually or via cron).'),
    '#options' => array(10 => '10', 50 => '50', 100 => '100', 200 => '200', -1 => t('unlimited')),
    '#default_value' => variable_get('sf_error_handling_per_process_number', 50),
  );
  $form['sf_error_handling']['sf_error_handling_prune_entries'] = array(
    '#type' => 'checkbox',
    '#title' => t('Prune duplicate entries from error queue upon each export.'),
    '#description' => t('Prune error queue entries automatically as soon as a successfull export of a matching object/mapping takes place. If this option is <strong>not</strong> selected, items will be kept in the queue until manually processed or processed by cron (which may be useful for simply logging errors).'),
    '#default_value' => variable_get('sf_error_handling_prune_entries', 1),
  );
  $form['sf_error_handling']['sf_error_handling_use_cron'] = array(
    '#type' => 'checkbox',
    '#title' => t('Process the error queue on cron.'),
    '#description' => t('Attempt to process the error queue on each cron run (subject to any per-processing limits set above).'),
    '#default_value' => variable_get('sf_error_handling_use_cron', 0),
  );
  $form['sf_error_handling']['sf_error_handling_cron_limit'] = array(
    '#type' => 'select',
    '#title' => t('Limit on the number of cron attempts for individual exports'),
    '#description' => t('The maximum number of times an individual export failure can be re-attempted during cron. Some items may always fail without manual intervention, so it can be a good idea to prevent cron from trying indefinitely.'),
    '#options' => array(5 => '5', 10 => '10', 20 => '20', 50 => '50', -1 => t('unlimited')),
    '#default_value' => variable_get('sf_error_handling_cron_limit', 10),
  );
}


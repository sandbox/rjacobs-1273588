<?php

/**
 * @file
 * Provides enhanced error handling/logging for failed Salesforce exports.
 *
 * This module monitors salesforce exports for errors and logs/queues any errors
 * it detects for later processing. It also implements cron processing of
 * errors.
 *
 * This module was originally concieved and authored by Ryan Jacobs (rjacobs).
 */

/**
 * Denotes a set of drupal object types and their export fucntions. This is
 * setup as a constant to make it easy for this module to be made aware of new
 * modules that define custom export functions for various objects in Drupal.
 * Any function defined here MUST use the following format:
 *
 * function export_function($drupal_oid, $fieldmap_name, $sfid)
 *
 * Export functions that do not use this format should not be added here but can
 * instead implement hook_sf_error_handling_export to make this module aware of
 * their unique export method.
 *
 */
define("EXPORT_FUNCTIONS", serialize(array(
      'user' => 'sf_user_export',
      'node' => 'sf_node_export',
      'uc_order' => 'uc_sf_order_export',
    )));

/**
 * Implementation of hook_salesforce_api_export_connect_fail().
 *
 * Here we catch export errors that resulted because of a Salesforce connection
 * failure.
 *
 * @param object $drupal_object
 *   The fully loaded Drupal object (node, user, etc)
 * @param string $name
 *   The name of the fieldmap used for export.
 * @param string $sfid
 *   The ID of the corresponding record in Salesforce.
 */
function sf_error_handling_salesforce_api_export_connect_fail($drupal_object, $name, $sfid) {
  // Numeric 0 indicates a connection failure.
  $response = 0;
  sf_error_handling_enqueue($drupal_object, $name, $sfid, $response);
}

/**
 * Implementation of hook_salesforce_api_post_export().
 *
 * Here we catch export errors that occured even though a salesforce connection
 * was successfully established. We may also attempt to prune the queue if
 * an export is successful that was previously in the queue.
 *
 * @param object $sf_object
 *   The object we attempted to send to Salesforce.
 * @param object $map
 *   The Salesforce fieldmap used for export.
 * @param object $drupal_object
 *   The fully-loaded Drupal object that we attempted to export.
 * @param object $response
 *   The error response received from Salesforce.
 */
function sf_error_handling_salesforce_api_post_export($sf_object, $map, $drupal_object, $response) {
  // Make sure we are working with the mapping object and not just the
  // fieldmap name.
  if (!is_object($map)) {
    $map = salesforce_api_fieldmap_load($map);
  }
  // Add any exports that failed due to an error returned explicitly from
  // Salesforce.
  if (!$response->success) {
    sf_error_handling_enqueue($drupal_object, $map, $response->id, $response);
  }
  // If we have a successful export, check the queue to see if this means
  // we can delete a matching entry there (if configured)
  elseif (variable_get('sf_error_handling_prune_entries', 1)) {
    $sql = 'SELECT *
        FROM {salesforce_errors}
        WHERE drupal_type = "%s" AND fieldmap_name = "%s" AND oid = %d';
    $match = db_fetch_array(db_query($sql, $map->drupal, $map->name, $drupal_object));
    if ($match) {
      db_query('DELETE FROM {salesforce_errors} WHERE id = %d', $match['id']);
    }
  }
}

/**
 * Add export errors to the queue (in database).
 *
 * @param object $oid
 *   The id of the Drupal object being exported.
 * @param object $map
 *   The salesforce fieldmap object for this export.
 * @param string $sfid
 *   The saleforce id for the salesforce object being exported to
 *   (if applicable).
 * @param string $responce
 *   The error responce for the failed export
 *
 * @return boolean
 *   Returns TRUE if an error was added to the queue. Returns FALSE if
 *   otherwise.
 */
function sf_error_handling_enqueue($oid, $map, $sfid, $response) {
  // Make sure we are working with the mapping object and not just the
  // fieldmap name.
  if (!is_object($map)) {
    $map = salesforce_api_fieldmap_load($map);
  }
  // Structure the error message based on the type of error being queued.
  // Will be int 0, not NULL, if connection failure
  if ($response === 0) {
    $error_msg = t('Salesforce connection failure');
  }
  elseif (is_array($response->errors)) {
    $error_msg = t('Export failure:') . '<br/><ul>';
    foreach ($response->errors as $error) {
      $error_msg .= '<li>' . $error->message . '</li>';
    }
    $error_msg .= '</ul>';
  }
  else {
    $error_msg = t('Unknown or exception before a Salesforce response could be returned (see watchdog)');
  }
  // Grab the variables to be written to the error queue
  $op = empty($sfid) ? 'create' : 'update';
  // @todo: figure out if there is an easy way to denote deletes
  $object = (object)array(
    'sf_op' => $op,
    'oid' => $oid,
    'error_count' => (int)1,
    'manual_attempts' => (int)0,
    'cron_attempts' => (int)0,
    'created' => time(),
    'updated' => time(),
    // will be blank on create
    'sfid' => empty($sfid) ? '' : $sfid,
    'drupal_type' => $map->drupal,
    'sf_type' => $map->salesforce,
    'name' => md5(microtime()),
    'fieldmap_name' => $map->name,
    'sf_data' => $sf_object,
    'error_msg' => $error_msg,
  );
  // Don't duplicate if a queue entry exists for this oid/mapping pair.
  $update = array();
  $sql = 'SELECT *
      FROM {salesforce_errors}
      WHERE drupal_type = "%s" AND fieldmap_name = "%s" AND oid = %d';
  $existing = db_fetch_array(db_query($sql, $map->drupal, $map->name, $oid));
  // If we find a match, adjust some entries accordingly.
  if ($existing) {
    $update = 'id';
    $object->id = $existing['id'];
    // Maintain create date
    $object->created = $existing['created'];
    // Increment error count
    $object->error_count = (int)$existing['error_count'] + 1;
    // Maintain manual attempt count (may be incremented later)
    $object->manual_attempts = $existing['manual_attempts'];
    // Maintain cron attempt count (may be incremented later)
    $object->cron_attempts = $existing['cron_attempts'];
  }
  // Record success or failure to the error export queue.
  if (drupal_write_record('salesforce_errors', $object, $update)) {
    if (user_access('administer salesforce')) {
      drupal_set_message(t('Drupal @type added or updated within the Salesforce error queue.', array('@type' => $map->drupal)), 'warning');
    }
    watchdog('sf_error_handling', t('Drupal @type added or updated within the Salesforce error queue.', array('@type' => $map->drupal)), NULL, WATCHDOG_WARNING);
    return TRUE;
  }
  else {
    watchdog('sf_error_handling', t('An item failed to be added to the salesforce export error queue.'), NULL, WATCHDOG_ERROR);
    return FALSE;
  }
}

/**
 * Process the queue.
 *
 * @param string $mode
 *   Indicates the "context" in which the error queue is being processed (via
 *   'cron' or 'manual').
 *
 * @return array
 *   Returns an associative array of the success and failure count for the items
 *   that were processed. Returns FALSE if no entries exist.
 */
function sf_error_handling_process_queue($mode = NULL) {
  $sf_error_count = db_result(db_query("SELECT count(created) FROM {salesforce_errors}"));
  if ($sf_error_count >= 1) {
    $count['success'] = 0;
    $count['fail'] = 0;
    $count['skipped'] = 0;
    $export_functions = unserialize(EXPORT_FUNCTIONS);
    $sf_error_handling_per_process_number = variable_get('sf_error_handling_per_process_number', 50);
    $sf_error_handling_cron_limit = variable_get('sf_error_handling_cron_limit', 10);
    $sf_error_items = db_query("SELECT * FROM {salesforce_errors} ORDER BY id");
    // Iterate through the queue and process each entry using the export
    // function that applies based on its type (building the export object from
    // scratch, etc.).
    $i = 0;
    while (($sf_error_item = db_fetch_object($sf_error_items)) && ($i < $sf_error_handling_per_process_number || $sf_error_handling_per_process_number == -1)) {
      $i++;
      $success = FALSE;
      // Skip this entry if this in a processing attempt called via cron and
      // we are over the cron attempt limit for this entry.
      if (($mode == "cron") && ($sf_error_item->cron_attempts >= $sf_error_handling_cron_limit || $sf_error_handling_cron_limit == -1)) {
        $count['skipped']++;
        continue;
      }
      // Many modules use a standard set of args in thier export function,
      // so we check our list of known fucntions (that used this
      // standard) and attempt an export if a match is found.
      if (array_key_exists($sf_error_item->drupal_type, $export_functions)) {
        $success = $export_functions[$sf_error_item->drupal_type]($sf_error_item->oid, $sf_error_item->fieldmap_name, $sf_error_item->sfid);
      }
      else {
        // If we are unaware of the export function for this drupal object type
        // invoke hook to see if any modules declare their unique export
        // methods.
        foreach (module_implements('sf_error_handling_export') as $module) {
          $function = $module . '_sf_error_handling_export';
          $success = $function($sf_error_item->drupal_type, $sf_error_item->oid, $sf_error_item->fieldmap_name, $sf_error_item->sfid);
          // As soon as we get a successful response, break out of the loop.
          if ($success) {
            break;
          }
        }
      }
      // If processing this entry resulted in a successful export, delete it
      // from the queue
      if ($success) {
        $count['success']++;
        db_query('DELETE FROM {salesforce_errors} WHERE id = %d', $sf_error_item->id);
      }
      else {
        $count['fail']++;
        // If we have a failure we need to record it in the correct database
        // column (to differentiate ways items may be triggered for processing).
        // By the time we get to this point, the 'error_count' value will have
        // already been incremented by one, so we need to adjust
        // accordingly.
        if ($mode == "manual") {
          db_query('UPDATE {salesforce_errors} SET manual_attempts=manual_attempts+1 WHERE id = %d', $sf_error_item->id);
          db_query('UPDATE {salesforce_errors} SET error_count=error_count-1 WHERE id = %d', $sf_error_item->id);
        }
        if ($mode == "cron") {
          db_query('UPDATE {salesforce_errors} SET cron_attempts=cron_attempts+1 WHERE id = %d', $sf_error_item->id);
          db_query('UPDATE {salesforce_errors} SET error_count=error_count-1 WHERE id = %d', $sf_error_item->id);
        }
      }
    }
    return $count;
  }
  return FALSE;
}

/**
 * Implementation of hook_cron().
 */
function sf_error_handling_cron() {
  // Process error queue via cron (if configured).
  if (variable_get('sf_error_handling_use_cron', 0)) {
    $count = sf_error_handling_process_queue("cron");
    if ($count === FALSE) {
      return;
    }
    watchdog('cron', t("sf_error_handling_cron: @success exports were successfully processed, @skipped skipped and @fail failed", array('@success' => $count['success'], '@skipped' => $count['skipped'], '@fail' => $count['fail'])), NULL, WATCHDOG_NOTICE);
  }
}

/**
 * Implementation of hook_menu().
 *
 * Here we set a form that allows an admin to view/process the queue along
 * with a form that will clear the error queue.
 */
function sf_error_handling_menu() {
  $items = array();
  $items['admin/settings/sf_errors'] = array(
    'title' => 'Salesforce export error log/queue',
    'description' => 'See and process the Salesforce export error queue.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('sf_error_handling_show_queue'),
    'access arguments' => array('administer salesforce'),
    'file' => 'sf_error_handling.admin.inc',
    // Let the queue be accessed directly from the admin menu
    'type' => MENU_NORMAL_ITEM,
  );
  $items['admin/settings/sf_errors/clear'] = array(
    'title' => 'Salesforce export export queue - clear queue',
    'description' => 'Clear the Salesforce export error queue.',
    'type' => MENU_CALLBACK,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('sf_error_handling_clear_queue'),
    'file' => 'sf_error_handling.admin.inc',
    'access arguments' => array('administer salesforce'),
  );
  return $items;
}


<?php

/**
 * @file
 * Install, update and uninstall functions for the af_error_handling module.
 */


/**
 * Implementation of hook_install().
 */
function sf_error_handling_install() {
  drupal_install_schema('sf_error_handling');
}

/**
 * Implementation of hook_uninstall().
 */
function sf_error_handling_uninstall() {
  drupal_uninstall_schema('sf_error_handling');
  db_query("DELETE FROM {variable} WHERE {name} LIKE 'sf_error_handling_%'");
}

/**
 * Implementation of hook_schema().
 */
function sf_error_handling_schema() {
  $schema['salesforce_errors'] = array(
    'description' => t('Salesforce objects queued for export due to errors'),
    'fields' => array(
      'sf_op' => array(
        'description' => 'The SalesForce export operation: "create", "update", or "delete"',
        'type' => 'varchar',
        'length' => 8,
        'not null' => TRUE,
        'default' => '',
      ),
      'id' => array(
        'description' => 'Primary key for this table.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'no export' => TRUE,
      ),
      'oid' => array(
        'description' => 'Specific Drupal object identifier (e.g. node id or user id)',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'error_count' => array(
        'description' => 'How many times has this specific export failed when triggered by the system.',
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
      ),
      'manual_attempts' => array(
        'description' => 'How many times a manual attempt to process this export failed.',
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
      ),
      'cron_attempts' => array(
        'description' => 'How many times a cron attempt to process this export failed.',
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
      ),
      'created' => array(
        'description' => 'Unix timestamp for this record\'s creation date',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'updated' => array(
        'description' => 'Unix timestamp for this record\'s update date',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'sfid' => array(
        'description' => 'Salesforce object identifier, if applicable',
        'type' => 'varchar',
        'length' => 32,
        'not null' => FALSE,
        'default' => '',
      ),
      'drupal_type' => array(
        'description' => 'Drupal object type (e.g. "node", "comment")',
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
        'default' => '',
      ),
      'sf_type' => array(
        'description' => 'SalesForce object type (e.g. "node", "comment")',
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
        'default' => '',
      ),
      'name' => array(
        'description' => 'Unique ID for this object. Used to identify it programatically.',
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
      ),
      'fieldmap_name' => array(
        'description' => 'Foreign key for salesforce_field_map - the fieldmap that corresponds to this record.',
        'type' => 'varchar',
        'length' => 64,
      ),
      'error_msg' => array(
        'description' => 'Details about this error.',
        'type' => 'text',
      ),
    ),
    'indexes' => array(
      'drupal_type' => array('drupal_type'),
      'sf_type' => array('sf_type'),
      'sf_op' => array('sf_op'),
      'created' => array('created'),
      'fieldmap_drupal' => array('fieldmap_name', 'drupal_type'),
    ),
    'unique keys' => array(
      'name' => array('name'),
    ),
    'primary key' => array('id'),
  );
  return $schema;
}

/*
 * Implementation of hook_requirements()
 * @param string $phase
 *  The phase in which hook_requirements is run:
 *    'install': the module is being installed.
 *    'runtime': the runtime requirements are being checked and shown on the status report page.
 * @return
 *   A keyed array of requirements. Each requirement is itself an array with the following items:
 *     'title': the name of the requirement.
 *     'value': the current value (e.g. version, time, level, ...).
 *       During install phase, this should only be used for version numbers, do not set it if not applicable.
 *     'description': description of the requirement/status.
 *     'severity': the requirement's result/severity level, one of:
 *       REQUIREMENT_INFO: For info only.
 *       REQUIREMENT_OK: The requirement is satisfied.
 *       REQUIREMENT_WARNING: The requirement failed with a warning.
 *       REQUIREMENT_ERROR: The requirement failed with an error.
 *
 */
function sf_error_handling_requirements($phase) {
  $requirements = array();

  if ($phase == 'runtime' && module_exists('sf_queue')) {
    $requirements[] = array(
      'title' => 'SF Queue enabled',
      'description' => 'This module is incompatible with SF Queue. Please disable SF Queue or this module.',
      'severity' => REQUIREMENT_ERROR,
    );
  }

  return $requirements;
}


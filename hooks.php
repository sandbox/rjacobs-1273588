<?php

/**
 * @file
 * These are the hooks that are invoked by the sf_error_handling module.
 *
 * @ingroup hooks
 */

/**
 * Respond to an attempt to re-process an export from the salesforce errors
 * queue.
 *
 * This hook is invoked from sf_error_handling.module during an attempt to
 * process the export error queue whenever the export method for a given drupal
 * object type is not known. This is a module's chance to declare its unique
 * export function so that sf_error_handling can use it whenever processing
 * items of the drupal type it enables salesforce exports for.
 *
 * @param string $drupal_type
 *   The drupal object type of the object to be exported to Salesforce.
 * @param int $oid
 *   The drupal id of the object to be exported to Salesforce.
 * @param string $fieldmap_name
 *   The salesforce fieldmap name for this export.
 * @param string $sfid
 *   The salesforce id (if known) to map this Drupal object to.
 *
 * @return boolean
 *   Returns TRUE if this module handled the export of this type AND an export
 *   successfully took place. Returns FALSE if otherwise.
 *
 * @ingroup hooks
 */
function hook_sf_error_handling_export($drupal_type, $oid, $fieldmap_name, $sfid) {
  if (strpos($drupal_type, 'node_webform') !== FALSE) {
    module_load_include('inc', 'webform', 'includes/webform.submissions');
    $submission = end(webform_get_submissions(array('sid' => $oid)));
    $node = node_load($submission->nid);
    return sf_webform_export($node, NULL, $sfid, $oid);
  }
  return FALSE;
}

